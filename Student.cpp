#include "Student.h"
#include "Course.h"

/*
Function inits the name, courses, crsCount.
Input:
The name, courses, crsCount.
Output:
None.
*/
void Student::init(std::string name, Course** courses, unsigned int crsCount)
{
	_name = name;
	this->_courses = courses;
	this->_crsCount = crsCount;
}

/*
Function gets the student's name.
Input:
None.
Output:
The student's name.
*/
std::string Student::getName()
{
	return _name;
}

/*
Function changes the student's name.
Input:
The name we want to change to.
Output:
None.
*/
void Student::setName(std::string name)
{
	_name = name;
}

/*
Function returns the courses number.
Input:
None.
Output:
the courses number.
*/
unsigned int Student::getCrsCount()
{
	return _crsCount;
}

/*
Function returns the courses list.
Input:
None.
Output:
The courses list.
*/
Course** Student::getCourses()
{
	return _courses;
}

/*
Function returns the grades average from all the courses.
Input:
None.
Output:
The grades average from all the courses.
*/
double Student::getAvg()
{
	double sum = 0;
	for (unsigned int i = 0; i < _crsCount; i++)
	{
		sum += this->_courses[i]->getFinalGrade();
	}

	return sum / _crsCount;
}
