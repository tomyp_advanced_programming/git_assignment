#include "myString.h"

/*
Fucntion gets a string and initting it.
Input:
The string.
Output:
None.
*/
void myString::init(std::string str)
{
	_strPtr = new std::string;
	(*_strPtr) = str;
}

/*
Fucntion returns the string.
*/
std::string myString::getStr()
{
	return (*_strPtr);
}

/*
Function returns the lenght of the string.
Input:
None.
Output:
The lenght of the string.
*/
unsigned int myString::getLength()
{
	return (*_strPtr).length();
}

/*
Function returns char from an index using [].
Input:
The index.
Output:
The char.
*/
char myString::getCharUsingSquareBrackets(unsigned int index)
{
	return (*_strPtr)[index];
}

/*
Function returns char from an index without using [].
Input:
The index.
Output:
The char.
*/
char myString::getCharUsingRoundBrackets(unsigned int index)
{
	return (*_strPtr).at(index);
}

/*
Fucntion gets the first char without using [].
Input:
None.
Output:
The fisrt char.
*/
char myString::getFirstChar()
{
	return (*_strPtr).at(0);
}

/*
Fucntion gets the last char without using [].
Input:
None.
Output:
The last char.
*/
char myString::getLastChar()
{
	return (*_strPtr).back();
}

/*
Function returns the index of the first occurrence.
Input:
The char - c.
Output:
The index.
*/
int myString::getCharFirstOccurrenceInd(char c)
{
	return (*_strPtr).find_first_of(c);
}

/*
Function returns the index of the last occurrence.
Input:
The char - c.
Output:
The index.
*/
int myString::getCharLastOccurrenceInd(char c)
{
	return (*_strPtr).find_last_of(c, (*_strPtr).length());
}

/*
Function returns the index of the first occurrence.
Input:
The string.
Output:
The index of the first occurrence.
*/
int myString::gstStringFirstOccurrenceInd(std::string str)
{
	return (*_strPtr).find(str);
}

/*
Fucntion compared between the strings and returns true/false.
Input:
The string.
Output:
true/false.
*/
bool myString::isEqual(std::string s)
{
	return (*_strPtr) == s ? true : false);
}