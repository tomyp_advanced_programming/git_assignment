#include "Course.h"

/*
Function inits the name, test1, test2, exam.
Input:
The name, test1, test2, exam.
Output:
None.
*/
void Course::init(std::string name, unsigned int test1, unsigned int test2, unsigned int exam)
{
	_gradeList = new unsigned int[3];
	_name = name;
	this->_test1 = test1;
	this->_test2 = test2;
	this->_exam = exam;
	_gradeList = getGradesList();
}


/*
Function returns the grades list.
Input:
None.
Output:
The grades list.
*/
unsigned int* Course::getGradesList()
{
	
	_gradeList[0] = this->_test1;
	_gradeList[1] = this->_test2;
	_gradeList[2] = this->_exam;
	return _gradeList;
}

/*
Fucntion returns the courses name.
Input:
None.
Output:
The courses name.
*/
std::string Course::getName()
{
	return _name;
}

/*
Function returns the final grade.
Input:
None.
Output:
The final grade.
*/
double Course::getFinalGrade()
{
	int _ans = 0;

	_ans = ((_test1*0.25) + (_test2*0.25) + (_exam*0.5));
	

	return (_ans);
}